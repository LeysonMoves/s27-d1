let http = require("http");

// Mock database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Robert",
		"email": "jobert@mail.com"
	}
];

http.createServer(function(request, response){
	// GET Request from the mock DB
	if(request.url == "/users" && request.method == "GET" ){
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write(JSON.stringify(directory));
		response.end();
	}

	// POST Request to the mock DB
	if(request.url == "/users" && request.method == "POST"){
		let requestBody = '';
		// Stream - a sequence of data
		request.on('data', function(data){
			// Assigns the data retrieved from the data stream to requestBody
			requestBody += data;
		});

		// response and step - only runs after request has completely been sent
		request.on('end', function(){
			// Chevks the data type of requestBody
			console.log(typeof requestBody);
			// Converts the string requestBody to JSON
			requestBody = JSON.parse(requestBody);
			// Create a new object representing the mock database record
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}

			// Add the new user to the mock database
			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type': 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();

		});
	}
}).listen(4000);
	

console.log('Server is running at localhost:4000');