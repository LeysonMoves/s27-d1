let http = require("http");

http.createServer(function(request, response) {
	if(request.url == "/items" && request.method == "GET"){
		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end('Data retrieved from database');
	}

	// POST Request
	if(request.url == "/items" && request.method == "POST"){
		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end('Data to be sent to the database');
	}

// PUT Request
	if(request.url == "/items" && request.method == "PUT"){
		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end('Data to be updated to the database');
	}

	// DELETE Request
	if(request.url == "/items" && request.method == "DELETE"){
		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end('Data to be deleted to the database');
	}


}).listen (4000);

console.log('Server is running at localhost: 4000');